package io.techchamps.restbackend.response;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class FileUploadResponse {

    private String fileName;

    private Long size;

    private String downloadUri;


}

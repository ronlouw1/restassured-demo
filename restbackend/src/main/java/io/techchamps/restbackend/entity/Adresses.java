package io.techchamps.restbackend.entity;

import lombok.*;


import javax.persistence.*;

@Entity
@Table(name = "adresses")
@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
public class Adresses {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String  description;

    private String country;
    private String city;
    private String street;
    private int houseNumber;
    private String zipCode;

}

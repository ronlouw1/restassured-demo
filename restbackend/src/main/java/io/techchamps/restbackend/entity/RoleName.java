package io.techchamps.restbackend.entity;

public enum RoleName {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN

}

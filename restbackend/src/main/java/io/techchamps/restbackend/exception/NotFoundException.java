package io.techchamps.restbackend.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException{

    private static final long SERIAL_VERSION_UUID = 1L;
    private final String message;

    public NotFoundException(String message){
        this.message = message;
    }
}

package io.techchamps.restbackend.controller;

import io.techchamps.restbackend.entity.User;
import io.techchamps.restbackend.exception.NotFoundException;
import io.techchamps.restbackend.repository.RoleRepository;
import io.techchamps.restbackend.response.UserResponse;
import io.techchamps.restbackend.services.UserServices;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/no_auth")
public class NoAuthApi {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    UserServices userServices;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @GetMapping(value = "/users")
    public List<UserResponse> getAllUsers() {
        List < User > listofUser = userServices.getAllUsers();
        List <UserResponse> userResponseList = new ArrayList<>();
        for (User user: listofUser ) {
            userResponseList.add(modelMapper.map(user, UserResponse.class));
        }

        return  userResponseList;
    }

    @GetMapping(value = "/users/{id}")
    public ResponseEntity<UserResponse> getuserById(@PathVariable("id") @Min(1) int id) {
        if (userServices.findById(id).isPresent()) {
            Optional<User> user = userServices.findById(id);
            UserResponse userResponse = modelMapper.map(user,UserResponse.class);
            return ResponseEntity.ok().body(userResponse);
        } else {
            throw new NotFoundException("User with " + id + " not found!");
        }
    }

    @GetMapping(value = "/users/email")
    public ResponseEntity<UserResponse>  getuserByEmail(@RequestParam(value = "email") String email) {

        if (userServices.findByEmail(email).isPresent()) {
            Optional<User> user = userServices.findByEmail(email);
            UserResponse userResponse = modelMapper.map(user,UserResponse.class);
            return ResponseEntity.ok().body(userResponse);
        } else {
            throw new NotFoundException("User with " + email + " not found!");
        }
    }
}

package io.techchamps.restbackend.controller;

import io.techchamps.restbackend.entity.Adresses;
import io.techchamps.restbackend.entity.Role;
import io.techchamps.restbackend.entity.RoleName;
import io.techchamps.restbackend.entity.User;
import io.techchamps.restbackend.exception.NotFoundException;
import io.techchamps.restbackend.jwt.JwtProvider;
import io.techchamps.restbackend.repository.AdressesRepository;
import io.techchamps.restbackend.repository.RoleRepository;
import io.techchamps.restbackend.repository.UserRepository;
import io.techchamps.restbackend.request.AdressRequest;
import io.techchamps.restbackend.request.LoginRequest;
import io.techchamps.restbackend.request.SignUpRequest;
import io.techchamps.restbackend.response.JwtResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/auth")
public class AuthAPI {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AdressesRepository adressesRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;



    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<String> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        User user1 = modelMapper.map(signUpRequest, User.class);

        user1.setPassword(encoder.encode(user1.getPassword()));
        for (AdressRequest adressRequest:signUpRequest.getAdresses()){
            Adresses adresses = modelMapper.map(adressRequest,Adresses.class);
           adressesRepository.save(adresses);
        }

        if (Boolean.TRUE.equals(userRepository.existsByUsername(signUpRequest.getUsername()))) {
            return new ResponseEntity<>("Fail -> Username is already taken!",
                    HttpStatus.BAD_REQUEST);
        }

        if (Boolean.TRUE.equals(userRepository.existsByEmail(signUpRequest.getEmail()))) {
            return new ResponseEntity<>("Fail -> Email is already in use!",
                    HttpStatus.BAD_REQUEST);
        }
        //Give user default role user
        Set<Role> roles = new HashSet<>();
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new NotFoundException("ERROR: Role not found"));
        roles.add(userRole);
        user1.setRoles(roles);
        try {
        userRepository.save(user1);

            return ResponseEntity.ok().body("User registered successfully!");
        }catch (Exception e){
            return ResponseEntity.badRequest().body("De opgegeven rol is niet toegestaan");
        }
    }

}

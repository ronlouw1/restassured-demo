package io.techchamps.restbackend.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdressRequest {

    private String  description;

    private String country;
    private String city;
    private String street;
    private int houseNumber;
    private String zipCode;
}

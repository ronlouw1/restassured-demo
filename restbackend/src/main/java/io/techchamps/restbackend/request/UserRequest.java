package io.techchamps.restbackend.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class UserRequest {

    private String name;
    private String username;
    private String email;
    private Set<String> roles;
    private List<AdressRequest> adresses;
}

import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {AuthenticationService} from "../services/authentication.service";
import {HomeClient} from "../clients/home.client";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public user: Observable<any> = this.homeClient.getUserData();

  constructor(
    private authenticationService: AuthenticationService,
    private homeClient: HomeClient
  ) { }

  ngOnInit(): void {
    }
  logout(): void{
    this.authenticationService.logout();
  }

}

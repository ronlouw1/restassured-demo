import { Injectable } from '@angular/core';
import {AuthenticationClient} from "../clients/authentication.client";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private tokenKey = 'token';
  private currentUser ='user';


  constructor(
    private authenticationClient: AuthenticationClient,
    private router: Router
  ) {}

  public login(username: string, password: string): void {
    this.authenticationClient.login(username, password).subscribe((response) => {
      //get token out of reply and store it!
     localStorage.setItem(this.tokenKey, response.token);
     localStorage.setItem(this.currentUser,username);

      this.router.navigate(['/']);
    });
  }
  public register(name: string ,username: string, email: string, password: string): void {
    this.authenticationClient
      .register(name,username,email,password)
      .subscribe((token) => {
        localStorage.setItem(this.tokenKey,token);
        this.router.navigate(['/']);
      });
  }
  public logout(){
    localStorage.removeItem(this.tokenKey);
    this.router.navigate(['/login'])
  }

  public isLoggedIn(): boolean{
    let token = localStorage.getItem(this.tokenKey);
    return token !=null && token.length >0;
  }

  public getToken(): string | null{
    return this.isLoggedIn() ? localStorage.getItem(this.tokenKey) : null;
  }
}
